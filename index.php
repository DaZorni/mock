<?php
ini_set('display_errors', 1);
use Zend\XmlRpc\Client;

require __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__. '/config.yml'));

$app->get('/getPackages/{name}', function ($name) use ($app)
{
    if (empty($app['servers'][$name])) {
        throw new \Exception('No server named "'. $name . '" configured' );
    };

    // data from config.yml
    $server     = $app['servers'][$name];
    $parameters = $server['parameters'];
    $userData   = $server['user_parameters'];
    unset($parameters['userId']);

    try {

        $client = new Zend\XmlRpc\Client("http://{$server['address']}/xmlrpcapi");
        $client->setSkipSystemLookup(true);

        $http_client = $client->getHttpClient();

        $http_client->setAuth($userData['username'], $userData['password'], \Zend\Http\Client::AUTH_BASIC);
        $client->setHttpClient($http_client);

        $packages = $client->call('getPackages', $parameters);

        return $app['twig']->render('response.html.twig', array(
            'method'        => 'getPackages',
            'name'          => $name,
            'callingParams' => $parameters,
            'returnParams'  => $packages
        ));

    } catch (\Exception $e ) {
        $msg = 'problem with the shop Api: '. $e->getMessage();
        $msg .= "<br>You might want to <a href='http://{$server['address']}'> look to sai </a>";

        return $msg;
    }
});

$app->get('/getSpecials/{name}', function ($name) use ($app)
{
    if (empty($app['servers'][$name])) {
        throw new \Exception('No server named "'. $name . '" configured' );
    };

    // data from config.yml
    $server     = $app['servers'][$name];
    $parameters = $server['parameters'];
    $userData   = $server['user_parameters'];
    unset($parameters['userId']);

    try {

        $client = new Zend\XmlRpc\Client("http://{$server['address']}/xmlrpcapi");
        $client->setSkipSystemLookup(true);

        $http_client = $client->getHttpClient();

        $http_client->setAuth($userData['username'], $userData['password'], \Zend\Http\Client::AUTH_BASIC);
        $client->setHttpClient($http_client);

        $packages = $client->call('getSpecials', $parameters);

        return $app['twig']->render('response.html.twig', array(
            'method'        => 'getSpecials',
            'name'          => $name,
            'callingParams' => $parameters,
            'returnParams'  => $packages
        ));

    } catch (\Exception $e ) {

        $msg = 'problem with the shop Api: '. $e->getMessage();
        $msg .= "<br>You might want to <a href='http://{$server['address']}'> look to sai </a>";

        return $app['twig']->render('response.html.twig', array(
            'method'        => 'getSpecials',
            'name'          => $name,
            'callingParams' => $parameters,
            'returnParams'  => $msg
        ));
    }
});

$app->get('/setReceipt/{name}', function ($name) use ($app)
{
    if (empty($app['servers'][$name])) {
        throw new \Exception('No server named "'. $name . '" configured' );
    };

    // data from config.yml
    $server     = $app['servers'][$name];

    try {

        $client = new Zend\XmlRpc\Client("http://{$server['address']}/xmlrpcapi");
        $client->setSkipSystemLookup(true);

        $http_client = $client->getHttpClient();

        $parameters = $server['parameters'];
        $userData   = $server['user_parameters'];
        $receipt    = $server['receipt'];

        $http_client->setAuth($userData['username'], $userData['password'], \Zend\Http\Client::AUTH_BASIC);
        $client->setHttpClient($http_client);

        $appData       = json_encode($parameters);
        $callingParams = array($receipt, $appData);

        $result  = $client->call('setReceipt', $callingParams);

        return $app['twig']->render('response.html.twig', array(
            'method'        => 'setReceipt',
            'name'          => $name,
            'callingParams' => $callingParams,
            'returnParams'  => $result
        ));

    } catch (\Exception $e ) {
        $msg = 'problem with the shop Api: '. $e->getMessage();
        $msg .= "<br>You might want to <a href='http://{$server['address']}'> look to sai </a>";

        return $app['twig']->render('response.html.twig', array(
            'method'        => 'setReceipt',
            'name'          => $name,
            'callingParams' => $callingParams,
            'returnParams'  => $msg
        ));
    }
});


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/public/views',
    'twig.class_path' => __DIR__ . '/vendor/twig/lib',
));

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array(
        'app' => $app['servers'],
    ));
});

$app->run();
